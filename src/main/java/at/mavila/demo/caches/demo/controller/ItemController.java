package at.mavila.demo.caches.demo.controller;

import at.mavila.demo.caches.demo.dto.Item;
import at.mavila.demo.caches.demo.service.ItemService;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import java.time.Instant;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@CacheConfig(cacheNames = "myitems")
@Slf4j
public class ItemController {


  private final ItemService itemService;

  public ItemController(final ItemService itemService) {
    this.itemService = itemService;
  }

  @GetMapping("/all_cached")
  @Cacheable
  public ResponseEntity<List<Item>> getAllCached() {
    return new ResponseEntity<>(this.itemService.getAll(), HttpStatus.OK);
  }

  @GetMapping("/all_uncached")
  public ResponseEntity<List<Item>> getAllUnCached() {
    return new ResponseEntity<>(this.itemService.getAll(), HttpStatus.OK);
  }

  @CacheEvict(allEntries = true)
  @Scheduled(cron = "*/60 * * * * *")
  public void cleanCache() {
    log.info("Cleaned cached at: <{}>. For demo purposes insert more random data", Instant.now());
    List<Item> insert = this.itemService.insert();
    log.info("Inserted: <{}>", insert.size());
  }

  @PreDestroy
  public void destroy() {
    log.info("Destroying bean of type <{}> at <{}>.", this.getClass().getSimpleName(), Instant.now());
  }

  @PostConstruct
  public void postConstruct() {
    List<Item> insert = this.itemService.insert();
    log.info("Constructing bean of type <{}> at <{}>. Elements inserted: <{}>", this.getClass().getSimpleName(), Instant.now(),
        insert.size());
  }
}