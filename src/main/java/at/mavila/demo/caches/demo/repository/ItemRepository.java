package at.mavila.demo.caches.demo.repository;

import at.mavila.demo.caches.demo.dto.Item;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ItemRepository extends JpaRepository<Item, Long> {
  List<Item> findByActive(Boolean active);

  @Query(" from Item i order by i.id asc")
  List<Item> findAllOrderById();

}
